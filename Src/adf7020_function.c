#include "adf7020_function.h"
#include "main.h"
#include "stm32f0xx_hal.h"

extern SPI_HandleTypeDef hspi1;

union ADFRegType {
	uint8_t w_data_matrix[4];
	uint32_t w_data;
};

void ADF7020_Reg_write(uint32_t data, uint8_t RegAdd) {
	union ADFRegType typetemp;
	uint8_t temp[4];
	data = (data&0xfff0)|((uint32_t)RegAdd&0x000f);
	typetemp.w_data = data;
	for(uint8_t i=0; i<4;i++ ) {
		*(temp+i)=*(typetemp.w_data_matrix+3-i);
	}
	CS_GPIO_Port->BSRR = (uint32_t)CS_Pin << 16U;
	HAL_Delay(10);
	HAL_SPI_Transmit(&hspi1, temp, 4, 1000);
	CS_GPIO_Port->BSRR = (uint32_t)CS_Pin;
	HAL_Delay(10);
}

void ADF7020_Reg_read(uint16_t* data, uint8_t RegAdd) {
	union ADFRegType typetemp;
	uint8_t temp[4];

}
